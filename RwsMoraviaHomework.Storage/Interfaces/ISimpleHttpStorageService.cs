﻿using System.Threading.Tasks;
using RwsMoraviaHomework.Storage.Interfaces.General;

namespace RwsMoraviaHomework.Storage.Interfaces
{
    public interface ISimpleHttpStorageService : IStorageRead
    {
        Task<string> ReadAsync(string path);
    }
}