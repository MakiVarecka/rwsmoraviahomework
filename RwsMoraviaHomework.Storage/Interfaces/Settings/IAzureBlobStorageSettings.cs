﻿using System;

namespace RwsMoraviaHomework.Storage.Interfaces.Settings
{
    public interface IAzureBlobStorageSettings
    {
        Uri ContainerAddress { get; }
        string AccountName { get; }
        string KeyValue { get; }
    }
}