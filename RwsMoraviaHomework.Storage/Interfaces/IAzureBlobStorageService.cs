﻿using System.Threading.Tasks;
using RwsMoraviaHomework.Storage.Interfaces.General;

namespace RwsMoraviaHomework.Storage.Interfaces
{
    public interface IAzureBlobStorageService : IStorageRead, IStorageWrite
    {
        Task<string> ReadAsync(string path);
        Task WriteAsync(string path, string value);
    }
}