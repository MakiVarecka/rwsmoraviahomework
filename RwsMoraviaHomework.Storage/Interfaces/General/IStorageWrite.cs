﻿namespace RwsMoraviaHomework.Storage.Interfaces.General
{
    public interface IStorageWrite
    {
        void Write(string path, string value);
    }
}