﻿namespace RwsMoraviaHomework.Storage.Interfaces.General
{
    public interface IStorageRead
    {
        string Read(string path);
    }
}