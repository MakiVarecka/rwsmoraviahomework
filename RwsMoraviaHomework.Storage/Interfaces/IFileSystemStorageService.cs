﻿using RwsMoraviaHomework.Storage.Interfaces.General;

namespace RwsMoraviaHomework.Storage.Interfaces
{
    public interface IFileSystemStorageService : IStorageRead, IStorageWrite
    {
        
    }
}