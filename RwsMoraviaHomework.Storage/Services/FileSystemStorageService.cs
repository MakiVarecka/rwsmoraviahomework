﻿using System.IO;
using RwsMoraviaHomework.Storage.Exceptions;
using RwsMoraviaHomework.Storage.Interfaces;

namespace RwsMoraviaHomework.Storage.Services
{
    public class FileSystemStorageService : IFileSystemStorageService
    {
        public string Read(string path)
        {
            if (!File.Exists(path))
                throw new NotFoundException($"File {path} not found.");

            using var stream = File.Open(path, FileMode.Open);
            {
                using var reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
        }

        public void Write(string path, string value)
        {
            using var stream = File.Open(path, FileMode.Create, FileAccess.Write);
            {
                using var writer = new StreamWriter(stream);
                writer.Write(value);
            }
        }
    }
}
