﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using RwsMoraviaHomework.Storage.Exceptions;
using RwsMoraviaHomework.Storage.Interfaces;
using RwsMoraviaHomework.Storage.Interfaces.Settings;

namespace RwsMoraviaHomework.Storage.Services
{
    public class AzureBlobStorageService : IAzureBlobStorageService
    {
        private readonly CloudBlobContainer _rwsContainerPublicAccess;
        private readonly CloudBlobContainer _rwsContainerPrivateAccess;

        public AzureBlobStorageService(IAzureBlobStorageSettings settings)
        {
            _rwsContainerPublicAccess = new CloudBlobContainer(settings.ContainerAddress);
            _rwsContainerPrivateAccess = new CloudBlobContainer(settings.ContainerAddress, new StorageCredentials(settings.AccountName, settings.KeyValue));
        }

        public string Read(string path)
        {
            var blob = _rwsContainerPublicAccess.GetBlockBlobReference(path);

            if (!blob.Exists())
                throw new NotFoundException($"File {blob.Uri.AbsoluteUri} not found.");

            return blob.DownloadText();
        }

        public async Task<string> ReadAsync(string path)
        {
            var blob = _rwsContainerPublicAccess.GetBlockBlobReference(path);

            if (!await blob.ExistsAsync())
                throw new NotFoundException($"File {blob.Uri.AbsoluteUri} not found.");

            return await blob.DownloadTextAsync();
        }

        public void Write(string path, string value)
        {
            var blob = _rwsContainerPrivateAccess.GetBlockBlobReference(path);
            blob.UploadText(value);
            Console.WriteLine(blob.Uri.AbsoluteUri);
        }

        public async Task WriteAsync(string path, string value)
        {
            var blob = _rwsContainerPrivateAccess.GetBlockBlobReference(path); 
            await blob.UploadTextAsync(value);
            Console.WriteLine(blob.Uri.AbsoluteUri);
        }
    }
}
