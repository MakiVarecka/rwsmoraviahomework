﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using RwsMoraviaHomework.Storage.Exceptions;
using RwsMoraviaHomework.Storage.Interfaces;

namespace RwsMoraviaHomework.Storage.Services
{
    /// <summary>
    /// This is just SIMPLY http get service
    /// </summary>
    public class SimpleHttpStorageService : ISimpleHttpStorageService
    {
        public string Read(string path)
        {
            return ReadAsync(path).GetAwaiter().GetResult();
        }

        public async Task<string> ReadAsync(string path)
        {
            if (!Uri.TryCreate(path, UriKind.Absolute, out var uri))
                throw new UriFormatException();

            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var response = await new HttpClient().SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            throw new NotFoundException($"File {path} not found.");
        }
    }
}
