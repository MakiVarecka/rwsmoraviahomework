﻿using Microsoft.Extensions.DependencyInjection;
using RwsMoraviaHomework.Storage.Interfaces;
using RwsMoraviaHomework.Storage.Services;

namespace RwsMoraviaHomework.Storage.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddStorages(this IServiceCollection services)
        {
            services.AddScoped<IAzureBlobStorageService, AzureBlobStorageService>();
            services.AddScoped<IFileSystemStorageService, FileSystemStorageService>();
            services.AddScoped<ISimpleHttpStorageService, SimpleHttpStorageService>();

            return services;
        }
    }
}
