﻿namespace RwsMoraviaHomework.Converter.Interfaces
{
    public interface IConverterDeserializer
    {
        T Deserialize<T>(string value);
    }
}