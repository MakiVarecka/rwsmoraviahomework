﻿namespace RwsMoraviaHomework.Converter.Interfaces
{
    public interface IConverterSerializer
    {
        string Serialize<T>(T model);
    }
}