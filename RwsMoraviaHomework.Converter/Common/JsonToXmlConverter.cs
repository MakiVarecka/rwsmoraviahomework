﻿using RwsMoraviaHomework.Converter.Providers;

namespace RwsMoraviaHomework.Converter.Common
{
    public class JsonToXmlConverter : BasicConverter<JsonConverterProvider, XmlConverterProvider> { }
}