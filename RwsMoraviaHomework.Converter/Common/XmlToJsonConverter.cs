﻿using System.Text.Json;
using RwsMoraviaHomework.Converter.Providers;

namespace RwsMoraviaHomework.Converter.Common
{
    public class XmlToJsonConverter : BasicConverter<XmlConverterProvider, JsonConverterProvider>
    {
        public XmlToJsonConverter(bool camelCase = false)
        {
            if (camelCase)
            {
                SetCamelCaseOn();
            }
            else
            {
                SetCamelCaseOff();
            }
        }

        public void SetCamelCaseOn()
        {
            ToConverter.JsonSerializerOptions = new JsonSerializerOptions
            {
                WriteIndented = true,
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };
        }

        public void SetCamelCaseOff()
        {
            ToConverter.JsonSerializerOptions = new JsonSerializerOptions
            {
                WriteIndented = true,
                PropertyNameCaseInsensitive = true,
                PropertyNamingPolicy = null
            };
        }
    }
}
