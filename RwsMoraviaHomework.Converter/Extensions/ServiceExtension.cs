﻿using Microsoft.Extensions.DependencyInjection;
using RwsMoraviaHomework.Converter.Common;

namespace RwsMoraviaHomework.Converter.Extensions
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddConverters(this IServiceCollection services)
        {
            services.AddSingleton<JsonToXmlConverter>();
            services.AddSingleton<XmlToJsonConverter>();

            return services;
        }
    }
}
