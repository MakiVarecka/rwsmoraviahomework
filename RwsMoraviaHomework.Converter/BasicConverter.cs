﻿using RwsMoraviaHomework.Converter.Base;
using RwsMoraviaHomework.Converter.Interfaces;

namespace RwsMoraviaHomework.Converter
{
    public class BasicConverter<TFrom, TTo> : ConverterBase<TFrom, TTo>
        where TFrom : IConverterDeserializer, new()
        where TTo : IConverterSerializer, new()
    {

        public BasicConverter() : base(new TFrom(), new TTo()) { }

        public override string Convert<TDocument>(string value)
        {
            return ToConverter.Serialize(FromConverter.Deserialize<TDocument>(value));
        }
    }
}
