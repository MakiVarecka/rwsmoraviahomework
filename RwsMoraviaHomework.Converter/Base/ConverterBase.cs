﻿using RwsMoraviaHomework.Converter.Interfaces;

namespace RwsMoraviaHomework.Converter.Base
{
    public abstract class ConverterBase<TFrom, TTo>
        where TFrom : IConverterDeserializer
        where TTo : IConverterSerializer
    {
        protected readonly TFrom FromConverter;
        protected readonly TTo ToConverter;

        protected ConverterBase(TFrom fromConverter, TTo toConverter)
        {
            FromConverter = fromConverter;
            ToConverter = toConverter;
        }

        public abstract string Convert<T>(string value);
    }
}