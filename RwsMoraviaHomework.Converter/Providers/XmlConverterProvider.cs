﻿using System.IO;
using System.Xml.Serialization;
using RwsMoraviaHomework.Converter.Exceptions;
using RwsMoraviaHomework.Converter.Interfaces;

namespace RwsMoraviaHomework.Converter.Providers
{
    public class XmlConverterProvider : IConverterDeserializer, IConverterSerializer
    {
        public T Deserialize<T>(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new NullOrEmptyException();

            using var reader = new StringReader(value);
            return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
        }

        public string Serialize<T>(T model)
        {
            using var writer = new StringWriter();
            new XmlSerializer(typeof(T)).Serialize(writer, model);
            return writer.ToString();
        }
    }
}