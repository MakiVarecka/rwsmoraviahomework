﻿using System.Text.Json;
using RwsMoraviaHomework.Converter.Exceptions;
using RwsMoraviaHomework.Converter.Interfaces;

namespace RwsMoraviaHomework.Converter.Providers
{
    public class JsonConverterProvider : IConverterDeserializer, IConverterSerializer
    {
        public JsonSerializerOptions JsonSerializerOptions = new JsonSerializerOptions
        {
            WriteIndented = true,
            PropertyNameCaseInsensitive = true
        };

        public T Deserialize<T>(string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new NullOrEmptyException();

            return JsonSerializer.Deserialize<T>(value, JsonSerializerOptions);
        }

        public string Serialize<T>(T model)
        {
            return JsonSerializer.Serialize(model, JsonSerializerOptions);
        }
    }
}