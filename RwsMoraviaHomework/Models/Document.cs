﻿using System.Xml.Serialization;

namespace RwsMoraviaHomework.Models
{
    [XmlRoot("root")]
    public class Document
    {
        [XmlElement("title")]
        public string Title { get; set; }
        [XmlElement("text")]
        public string Text { get; set; }
    }
}
