﻿using System;
using RwsMoraviaHomework.Storage.Interfaces.Settings;

namespace RwsMoraviaHomework.Settings
{
    /// <summary>
    /// ONLY FOR DEMONSTRATION. THIS SHOULD BE STORED IN APPSETTINGS, CREDENTIAL MANAGER, KEY VAULTS etc.
    /// </summary>
    public class UglyAzureBlobStorageSettings : IAzureBlobStorageSettings
    {
        public Uri ContainerAddress => new Uri("https://makivareckatest.blob.core.windows.net/rws");
        public string AccountName => "makivareckatest";
        public string KeyValue => "y1pdUaPRbPvFa8gT0Obr24sfyDPTFXCobfaP+4PBLF4duq42crv1rETXGh6YzheQ/nieHE1XtpNGPqUub6uREg==";
    }
}