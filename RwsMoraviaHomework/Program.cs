﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using RwsMoraviaHomework.Converter.Extensions;
using RwsMoraviaHomework.Examples;
using RwsMoraviaHomework.Settings;
using RwsMoraviaHomework.Storage.Extensions;
using RwsMoraviaHomework.Storage.Interfaces.Settings;

namespace RwsMoraviaHomework
{
    internal class Program
    {
        private static IServiceProvider _serviceProvider;

        private static async Task Main(string[] args)
        {
            RegisterServices();

            // Examples of various conversions in file system
            var fileToFileExample = _serviceProvider.GetService<IFileSystemToFileSystemExample>();
            fileToFileExample.Process();

            // Examples of various conversions in azure blob storage
            var blobToBlobExample = _serviceProvider.GetService<IAzureBlobToAzureBlobExample>();
            await blobToBlobExample.Process();

            // Example of downloading file from azure blob storage, converting to json and saving in file system
            var blobToFileExample = _serviceProvider.GetService<IAzureBlobToFileSystem>();
            await blobToFileExample.Process();

            // Example of fetching file via HTTP GET request, converting to xml and saving in azure blob storage
            var httpToBlobExample = _serviceProvider.GetService<ISimpleHttpToAzureBlobExample>();
            await httpToBlobExample.Process();

            DisposeServices(); 
        }

        #region DI

        private static void RegisterServices()
        {
            var services = new ServiceCollection();

            services.AddSingleton<IAzureBlobStorageSettings, UglyAzureBlobStorageSettings>();
            
            services.AddStorages();
            services.AddConverters();

            services.AddScoped<IAzureBlobToAzureBlobExample, AzureBlobToAzureBlobExample>();
            services.AddScoped<IFileSystemToFileSystemExample, FileSystemToFileSystemExample>();
            services.AddScoped<IAzureBlobToFileSystem, AzureBlobToFileSystem>();
            services.AddScoped<ISimpleHttpToAzureBlobExample, SimpleHttpToAzureBlobExample>();
            
            _serviceProvider = services.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            switch (_serviceProvider)
            {
                case null:
                    return;
                case IDisposable disposable:
                    disposable.Dispose();
                    break;
            }
        }

        #endregion
    }
}
