﻿using System;
using System.Threading.Tasks;
using RwsMoraviaHomework.Converter.Common;
using RwsMoraviaHomework.Models;
using RwsMoraviaHomework.Storage.Interfaces;

namespace RwsMoraviaHomework.Examples
{
    public interface ISimpleHttpToAzureBlobExample
    {
        Task Process();
    }

    public class SimpleHttpToAzureBlobExample : ISimpleHttpToAzureBlobExample
    {
        private readonly ISimpleHttpStorageService _simpleHttpStorageService;
        private readonly IAzureBlobStorageService _azureBlobStorageService;
        private readonly JsonToXmlConverter _jsonToXmlConverter;

        public SimpleHttpToAzureBlobExample(ISimpleHttpStorageService simpleHttpStorageService,
            IAzureBlobStorageService azureBlobStorageService, JsonToXmlConverter jsonToXmlConverter)
        {
            _simpleHttpStorageService = simpleHttpStorageService;
            _azureBlobStorageService = azureBlobStorageService;
            _jsonToXmlConverter = jsonToXmlConverter;
        }

        public async Task Process()
        {
            const string path = "https://makivareckatest.blob.core.windows.net/rws/samples/document.json";
            var json = await _simpleHttpStorageService.ReadAsync(path);

            var xml = _jsonToXmlConverter.Convert<Document>(json);

            await _azureBlobStorageService.WriteAsync($"converted/{DateTime.Now:yyyyMMddHHmmss}-http2blob-json2xml.xml", xml);
        }
    }
}