﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using RwsMoraviaHomework.Converter.Common;
using RwsMoraviaHomework.Models;
using RwsMoraviaHomework.Storage.Interfaces;

namespace RwsMoraviaHomework.Examples
{
    public interface IAzureBlobToFileSystem
    {
        Task Process();
    }

    public class AzureBlobToFileSystem : IAzureBlobToFileSystem
    {
        private readonly IAzureBlobStorageService _azureBlobStorageService;
        private readonly IFileSystemStorageService _fileSystemStorageService;
        private readonly XmlToJsonConverter _xmlToJsonConverter;

        public AzureBlobToFileSystem(IAzureBlobStorageService azureBlobStorageService,
            IFileSystemStorageService fileSystemStorageService, XmlToJsonConverter xmlToJsonConverter)
        {
            _azureBlobStorageService = azureBlobStorageService;
            _fileSystemStorageService = fileSystemStorageService;
            _xmlToJsonConverter = xmlToJsonConverter;
        }

        public async Task Process()
        {
            var xml = await _azureBlobStorageService.ReadAsync("samples/document.xml");

            var appRootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var convertedPath = Path.Combine(appRootPath!, "converted");

            _xmlToJsonConverter.SetCamelCaseOn();
            var json = _xmlToJsonConverter.Convert<Document>(xml);
            _xmlToJsonConverter.SetCamelCaseOff();

            _fileSystemStorageService.Write(Path.Combine(convertedPath, $"{DateTime.Now:yyyyMMddHHmmss}-blob2fs-xml2json.json"), json);
        }
    }
}