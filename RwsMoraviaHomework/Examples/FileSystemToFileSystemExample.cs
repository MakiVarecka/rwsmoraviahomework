﻿using System;
using System.IO;
using System.Reflection;
using RwsMoraviaHomework.Converter;
using RwsMoraviaHomework.Converter.Common;
using RwsMoraviaHomework.Converter.Providers;
using RwsMoraviaHomework.Models;
using RwsMoraviaHomework.Storage.Interfaces;

namespace RwsMoraviaHomework.Examples
{
    public interface IFileSystemToFileSystemExample
    {
        void Process();
    }

    public class FileSystemToFileSystemExample : IFileSystemToFileSystemExample
    {
        private readonly IFileSystemStorageService _fileSystemStorageService;
        private readonly XmlToJsonConverter _xmlToJsonConverter;
        private readonly JsonToXmlConverter _jsonToXmlConverter;

        public FileSystemToFileSystemExample(IFileSystemStorageService fileSystemStorageService,
            XmlToJsonConverter xmlToJsonConverter, JsonToXmlConverter jsonToXmlConverter)
        {
            _fileSystemStorageService = fileSystemStorageService;
            _xmlToJsonConverter = xmlToJsonConverter;
            _jsonToXmlConverter = jsonToXmlConverter;
        }

        public void Process()
        {
            var appRootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var sampleXml = _fileSystemStorageService.Read(Path.Combine(appRootPath!, "Samples", "document.xml"));
            var sampleJson = _fileSystemStorageService.Read(Path.Combine(appRootPath!, "Samples", "document.json"));

            var timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            var convertedPath = Path.Combine(appRootPath!, "Converted");

            if (!Directory.Exists(convertedPath))
            {
                Directory.CreateDirectory(convertedPath);
            }

            #region XML to JSON

            var json1 = _xmlToJsonConverter.Convert<Document>(sampleXml);
            _fileSystemStorageService.Write(Path.Combine(convertedPath, $"{timestamp}-xml2json.json"), json1);

            #endregion

            #region XML to JSON camel-case
            _xmlToJsonConverter.SetCamelCaseOn();
            var json2 = _xmlToJsonConverter.Convert<Document>(sampleXml);
            _xmlToJsonConverter.SetCamelCaseOff();
            _fileSystemStorageService.Write(Path.Combine(convertedPath, $"{timestamp}-xml2json-camelcase.json"), json2);

            #endregion

            #region JSON to XML

            var xml1 = _jsonToXmlConverter.Convert<Document>(sampleJson);
            _fileSystemStorageService.Write(Path.Combine(convertedPath, $"{timestamp}-json2xml.xml"), xml1);

            #endregion

            #region Ad hoc converter (BasicConverter) - JSON to XML

            var basicConverter = new BasicConverter<JsonConverterProvider, XmlConverterProvider>();
            var xml2 = basicConverter.Convert<Document>(sampleJson);
            _fileSystemStorageService.Write(Path.Combine(convertedPath, $"{timestamp}-json2xml-basic.xml"), xml2);

            #endregion
        }
    }
}