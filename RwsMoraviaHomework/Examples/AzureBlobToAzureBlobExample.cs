﻿using System;
using System.Threading.Tasks;
using RwsMoraviaHomework.Converter;
using RwsMoraviaHomework.Converter.Common;
using RwsMoraviaHomework.Converter.Providers;
using RwsMoraviaHomework.Models;
using RwsMoraviaHomework.Storage.Interfaces;

namespace RwsMoraviaHomework.Examples
{
    public interface IAzureBlobToAzureBlobExample
    {
        Task Process();
    }

    public class AzureBlobToAzureBlobExample : IAzureBlobToAzureBlobExample
    {
        private readonly IAzureBlobStorageService _azureBlobStorageService;
        private readonly XmlToJsonConverter _xmlToJsonConverter;
        private readonly JsonToXmlConverter _jsonToXmlConverter;

        public AzureBlobToAzureBlobExample(IAzureBlobStorageService azureBlobStorageService,
            XmlToJsonConverter xmlToJsonConverter, JsonToXmlConverter jsonToXmlConverter)
        {
            _azureBlobStorageService = azureBlobStorageService;
            _xmlToJsonConverter = xmlToJsonConverter;
            _jsonToXmlConverter = jsonToXmlConverter;
        }

        public async Task Process()
        {
            var sampleXml = await _azureBlobStorageService.ReadAsync("samples/document.xml");
            var sampleJson = await _azureBlobStorageService.ReadAsync("samples/document.json");

            var timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            #region XML to JSON

            var json1 = _xmlToJsonConverter.Convert<Document>(sampleXml);
            await _azureBlobStorageService.WriteAsync($"converted/{timestamp}-xml2json.json", json1);

            #endregion

            #region XML to JSON camel-case

            _xmlToJsonConverter.SetCamelCaseOn();
            var json2 = _xmlToJsonConverter.Convert<Document>(sampleXml);
            _xmlToJsonConverter.SetCamelCaseOff();
            
            await _azureBlobStorageService.WriteAsync($"converted/{timestamp}-xml2json-camelcase.json", json2);

            #endregion

            #region JSON to XML

            var xml1 = _jsonToXmlConverter.Convert<Document>(sampleJson);
            await _azureBlobStorageService.WriteAsync($"converted/{timestamp}-json2xml.xml", xml1);

            #endregion

            #region Ad hoc converter (BasicConverter) - JSON to XML

            var basicConverter = new BasicConverter<JsonConverterProvider, XmlConverterProvider>();
            var xml2 = basicConverter.Convert<Document>(sampleJson);
            await _azureBlobStorageService.WriteAsync($"converted/{timestamp}-json2xml-basic.xml", xml2);

            #endregion
        }
    }
}