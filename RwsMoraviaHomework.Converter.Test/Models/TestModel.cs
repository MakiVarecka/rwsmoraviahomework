﻿using System;
using System.Collections.Generic;

namespace RwsMoraviaHomework.Converter.Test.Models
{
    public class TestModel
    {
        public bool Boolean { get; set; }
        public int Integer { get; set; }
        public double Double { get; set; }
        public decimal Decimal { get; set; }
        public DateTime DateTime { get; set; }
        public string Text { get; set; }
        public NestedTestModel NestedTestModel { get; set; }
        public List<NestedTestModel> ListOdNestedTestModels { get; set; }

        public TestModel() { }

        public TestModel(bool initialize = false)
        {
            if (!initialize) return;

            Boolean = true;
            Integer = 13;
            Double = 11.2d;
            Decimal = 85.45m;
            DateTime = DateTime.Now;
            Text = "Lorem ipsum dolor sit amet";
            NestedTestModel = new NestedTestModel(true);
            ListOdNestedTestModels = new List<NestedTestModel>
            {
                new NestedTestModel(true),
                new NestedTestModel(true),
                new NestedTestModel(true)
            };
        }
    }
}