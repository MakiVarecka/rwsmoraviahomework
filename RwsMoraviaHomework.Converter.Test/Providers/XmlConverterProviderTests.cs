﻿using System;
using FluentAssertions;
using RwsMoraviaHomework.Converter.Exceptions;
using RwsMoraviaHomework.Converter.Providers;
using RwsMoraviaHomework.Converter.Test.Models;
using Xunit;

namespace RwsMoraviaHomework.Converter.Test.Providers
{
    public class XmlConverterProviderTests
    {
        [Fact]
        public void Deserialize_ParameterValueIsNullOrEmpty_ThrowsArgumentNullException()
        {
            var converter = new XmlConverterProvider();

            Assert.Throws<NullOrEmptyException>(() => converter.Deserialize<NestedTestModel>(string.Empty));
            Assert.Throws<NullOrEmptyException>(() => converter.Deserialize<NestedTestModel>(null));
        }

        [Fact]
        public void Deserialize_IncompleteXml_ThrowsInvalidOperationException()
        {
            const string incompleteXml = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<NestedTestModel>\r\n" +
                                         "<Boolean>true</Boolean>\r\n<Integer>13</Integer>\r\n<Double>11.2</Double>\r\n" +
                                         "<Decimal>85.45</Decimal>\r\n<DateTime>2020-06-08T21:53:39.1111484+02:00</DateTime>\r\n" +
                                         "<Text>Lorem ipsum dolor sit amet</Text>\r\n"; // closure tag '</NestedTestModel>' is missing
            var converter = new XmlConverterProvider();

            Assert.Throws<InvalidOperationException>(() => converter.Deserialize<NestedTestModel>(incompleteXml));
        }

        [Fact]
        public void SerializeDeserialize_NoDataLoss_PropertiesOfObjectsAreEqual()
        {
            var inputModel = new TestModel(true);
            var converter = new XmlConverterProvider();

            var outputModel = converter.Deserialize<TestModel>(converter.Serialize(inputModel));

            outputModel.Should().BeEquivalentTo(inputModel);
        }
    }
}