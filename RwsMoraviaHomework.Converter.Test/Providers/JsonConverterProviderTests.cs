﻿using FluentAssertions;
using RwsMoraviaHomework.Converter.Exceptions;
using RwsMoraviaHomework.Converter.Providers;
using RwsMoraviaHomework.Converter.Test.Models;
using Xunit;

namespace RwsMoraviaHomework.Converter.Test.Providers
{
    public class JsonConverterProviderTests
    {
        [Fact]
        public void Deserialize_ParameterValueIsNullOrEmpty_ThrowsArgumentNullException()
        {
            var converter = new JsonConverterProvider();

            Assert.Throws<NullOrEmptyException>(() => converter.Deserialize<NestedTestModel>(string.Empty));
            Assert.Throws<NullOrEmptyException>(() => converter.Deserialize<NestedTestModel>(null));
        }

        [Fact]
        public void Deserialize_IncompleteJson_ThrowsJsonException()
        {
            const string incompleteJson = "{ \"Text\": \"Lorem ipsum dolor sit amet\""; // last '}' is missing
            var converter = new JsonConverterProvider();

            Assert.Throws<System.Text.Json.JsonException>(() => converter.Deserialize<NestedTestModel>(incompleteJson));
        }

        [Fact]
        public void SerializeDeserialize_NoDataLoss_PropertiesOfObjectsAreEqual()
        {
            var inputModel = new TestModel(true);
            var converter = new JsonConverterProvider();

            var outputModel = converter.Deserialize<TestModel>(converter.Serialize(inputModel));

            outputModel.Should().BeEquivalentTo(inputModel);
        }
    }
}