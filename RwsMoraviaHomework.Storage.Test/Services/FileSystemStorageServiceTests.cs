﻿using System;
using System.IO;
using System.Reflection;
using RwsMoraviaHomework.Storage.Exceptions;
using RwsMoraviaHomework.Storage.Services;
using Xunit;

namespace RwsMoraviaHomework.Storage.Test.Services
{
    public class FileSystemStorageServiceTests
    {
        [Fact]
        public void Read_NonExistingFile_ThrowsNotFoundException()
        {
            var storage = new FileSystemStorageService();
            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, $"{Guid.NewGuid()}.txt");

            Assert.Throws<NotFoundException>(() => storage.Read(path));
        }

        [Fact]
        public void Write_WriteTextToFile_WrittenTextIsEqual()
        {
            var storage = new FileSystemStorageService();
            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, $"{Guid.NewGuid()}.txt");
            const string text = "Lorem ipsum dolor sit amet";

            storage.Write(path, text);
            var result = storage.Read(path);

            Assert.Equal(text, result);
        }
    }
}