﻿using System;
using System.Threading.Tasks;
using Moq;
using RwsMoraviaHomework.Storage.Exceptions;
using RwsMoraviaHomework.Storage.Interfaces;
using Xunit;

namespace RwsMoraviaHomework.Storage.Test.Services
{
    public class AzureBlobStorageServiceTests
    {
        private readonly Mock<IAzureBlobStorageService> _mockStorage;

        public AzureBlobStorageServiceTests()
        {
            const string nonExistingPath = "rws/non-existing-blob";

            _mockStorage = new Mock<IAzureBlobStorageService>();
            _mockStorage.Setup(x => x.Read(It.IsAny<string>())).Returns("Lorem ipsum dolor sit amet");
            _mockStorage.Setup(x => x.ReadAsync(It.IsAny<string>())).ReturnsAsync("Lorem ipsum dolor sit amet");

            _mockStorage.Setup(x => x.Read(nonExistingPath)).Throws<NotFoundException>();
            _mockStorage.Setup(x => x.ReadAsync(nonExistingPath)).ThrowsAsync(new NotFoundException());

            _mockStorage.Setup(x => x.Write(It.IsAny<string>(), It.IsAny<string>()));
            _mockStorage.Setup(x => x.WriteAsync(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Fact]
        public void Read_NonExistingBlob_ThrowsNotFoundException()
        {
            const string path = "rws/non-existing-blob";
            var storage = _mockStorage.Object;

            Assert.Throws<NotFoundException>(() => storage.Read(path));
        }

        [Fact]
        public void ReadAsync_NonExistingBlob_ThrowsNotFoundException()
        {
            const string path = "rws/non-existing-blob";
            var storage = _mockStorage.Object;

            Assert.ThrowsAsync<NotFoundException>(async () => await storage.ReadAsync(path));
        }

        [Fact]
        public void Write_UploadTextToBlob_UploadedTextIsEqual()
        {
            var storage = _mockStorage.Object;
            var path = $"test/{Guid.NewGuid()}.txt";
            const string text = "Lorem ipsum dolor sit amet";

            storage.Write(path, text);
            var result = storage.Read(path);

            Assert.Equal(text, result);
        }

        [Fact]
        public async Task WriteAsync_UploadTextToBlob_UploadedTextIsEqual()
        {
            var storage = _mockStorage.Object;
            var path = $"test/{Guid.NewGuid()}.txt";
            const string text = "Lorem ipsum dolor sit amet";

            await storage.WriteAsync(path, text);
            var result = await storage.ReadAsync(path);

            Assert.Equal(text, result);
        }
    }
}