# RwsMoraviaHomework
- Author: Martin Vařečka
- Last change: 9. 6. 2020

## Project structure
- **RwsMoraviaHomework** - .Net Core 3.1 Console App
    - Examples - application examples (how to work with converters and storages)
    - Models - provided document model
    - Settings - hardcoded azure blob storage connection configuration (only for demonstration!)
    - Program.cs - dependency injection set up and examples processing
- **RwsMoraviaHomework.Converter** - .Net Core 3.1 Class Library (XML and JSON converters)
    - Base - ConverterBase.cs - abstract class for new converters
    - Common - pre prepared converters (commonly used)
    - Interfaces - interface for serialization and deserialization 
    - Providers - implemented converters logic
    - BasiConverter.cs - basic converter which implement abstract class ConverterBase.cs and consume Providers
- **RwsMoraviaHomework.Converter.Test** - xUnit tests for RwsMoraviaHomework.Converter (+nuget FluentAssertions)
- **RwsMoraviaHomework.Storage** - .Net Core 3.1 Class Library (File system, Azure Blob, Simple Http download)
    - Services
        - AzureBlobStorageService - implementation of downloading and uploading to/from Azure Blob Storage
        - FileSystemStorageService - working with document in filesystem
        - SimpleHttpStorageService - this is just sketch of simply http get request
- **RwsMoraviaHomework.Storage.Test** - xUnit tests for RwsMoraviaHomework.Storage (+nuget Moq)